﻿# WorkflowUpdateFolder

## Component Information
* Created by: Mythics
* Author: Jonathan Hult
* Last Updated: build_1_20140319
* License: MIT

## Overview
This WebCenter Content component allows a folder update in workflow. Folder updates can be done via the Idoc Script function wfUpdateMetadata. However, this function uses the service UPDATE_METADATA which only updates the latest revision. This causes content to appear to be in two different folders at the same time. Instead, we want the folder to be updated on every revision. If you try to call UPDATE_DOCINFO via the executeService function in a workflow step, it will run into a database transaction already in progress. To avoid this, we continue to call wfUpdateMetadata during the workflow step. We then hook in after the workflow approval via the serviceCleanUp filter so there is no longer a database transaction. The component determines if the folder has changed and if so, calls UPDATE_DOCINFO which updates every revision.

* FilterImplementors:
	- serviceCleanUp: Core - Runs at end of service calls (only hooks into WORKFLOW_APPROVE service).

* Tracing sections:
	- workflowupdatefolder
	
* Preference prompts:
	- WorkflowUpdateFolder_ComponentEnabled: boolean determining whether component functionality is enabled.
	- WorkflowUpdateFolder_FolderField: Folder field name. Example: xCollectionID or fFolderGUID

## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 11.1.1.8.1DEV-2014-01-06 04:18:30Z-r114490 (Build:7.3.5.185)

## Changelog
* build_1_20140319
	- Initial component release