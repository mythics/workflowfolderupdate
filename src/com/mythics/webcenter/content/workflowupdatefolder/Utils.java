package com.mythics.webcenter.content.workflowupdatefolder;

import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.DOC_INFO;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.D_DOC_ACCOUNT;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.D_DOC_NAME;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.D_ID;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.D_REV_LABEL;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.D_SECURITY_GROUP;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.FOLDER_FIELD;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.IDC_SERVICE;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.REMOTE_USER;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.REVISION_HISTORY;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.SYSADMIN;
import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.TRACE_SECTION;
import static intradoc.shared.SharedObjects.getEnvironmentValue;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataResultSet;
import intradoc.data.Workspace;
import intradoc.server.ServiceManager;

import com.mythics.webcenter.content.workflowupdatefolder.StringConstants.ServiceName;

/**
 * 
 * @author Jonathan Hult
 * 
 */
public class Utils {

	/**
	 * Check if folder value has changed (by retrieving DOC_INFO and determining
	 * if folderField value has changed). If so, call updateMetadata method.
	 * 
	 * @param originalDataBinder
	 *            Original DataBinder from filter.
	 * @param ws
	 *            Workspace from filter.
	 */
	static void doCheckAndUpdate(final DataBinder originalDataBinder, final Workspace ws) {
		traceVerbose("Enter doCheckAndUpdate");

		try {
			// Folder metadata field name
			final String folderField = getEnvironmentValue(FOLDER_FIELD);
			traceVerbose("folderField: " + folderField);

			if (folderField != null && !folderField.trim().isEmpty()) {

				// Get dID needed for DOC_INFO
				final String dID = originalDataBinder.getLocal(D_ID);
				trace("Current dID: " + dID);

				// Get new folder to be setup on revisions
				final String newFolderValue = originalDataBinder.getLocal(folderField);
				trace("newFolder: " + newFolderValue);

				if (newFolderValue != null) {
					try {
						// Get REVISION_HISTORY so we can get dID of previous
						// revision
						final DataResultSet revHistory = getRS(REVISION_HISTORY, dID, ws);

						// Get current folder for current revision - 1 (need to
						// go 1
						// revision back since latest has already been updated)
						if (revHistory.first() && revHistory.next()) {

							// Latest revision - 1 dID
							final String secondToLatestRevisiondID = revHistory.getStringValueByName(D_ID);
							trace("secondToLatestRevisiondID: " + secondToLatestRevisiondID);

							// Get DOC_INFO so we can get current folder
							final DataResultSet docInfo = getRS(DOC_INFO, secondToLatestRevisiondID, ws);

							// Get current folder for revision (need to go 1
							// revision back since latest has already been
							// updated)
							if (docInfo.first()) {
								// Get new folder to be setup on revisions
								final String originalFolderValue = docInfo.getStringValueByName(folderField);
								trace("originalFolderValue: " + originalFolderValue);

								if (originalFolderValue != null && !originalFolderValue.equals(newFolderValue)) {
									trace("Folder has been updated; need to set updated folder on all revisions");
									updateMetadata(originalDataBinder, folderField, newFolderValue, ws);
								} else {
									trace("Folders match; no update needed");
								}
							}
						}
					} catch (final ServiceException e) {
						warn("Could not retrieve DOC_INFO for dID: " + dID + "; no revisions will be updated with new folder", e);
					}
				}
			}
		} finally {
			traceVerbose("Exit doCheckAndUpdate");
		}
	}

	/**
	 * Execute a WebCenter Content service.
	 * 
	 * @param serviceBinder
	 *            DataBinder which contains the information for the service
	 *            call.
	 * @param workspace
	 *            Workspace from the WebCenter Content filter.
	 * @throws ServiceException
	 */
	static void executeService(final DataBinder serviceBinder, final Workspace workspace) throws ServiceException {
		traceVerbose("Enter executeService");

		try {
			// Retrieve the IdcService name so it can be traced
			final String idcService = serviceBinder.getLocal(IDC_SERVICE);

			traceVerbose(serviceBinder.getLocalData().toString());

			serviceBinder.setEnvironmentValue(REMOTE_USER, SYSADMIN);
			final ServiceManager smg = new ServiceManager();
			smg.init(serviceBinder, workspace);
			smg.processCommand();
			smg.clear();
			traceVerbose("Successfully executed IdcService " + idcService);
		} catch (final Exception e) {
			throw new ServiceException(e);
		} finally {
			traceVerbose("Exit executeService");
		}
	}

	/**
	 * Get ResultSet for specified dID.
	 * 
	 * @param rsName
	 *            ResultSet name to retrieve.
	 * @param dID
	 *            dID to retrieve DOC_INFO for.
	 * @param ws
	 *            Workspace from filter.
	 * @return ResultSet.
	 * @throws ServiceException
	 */
	static DataResultSet getRS(final String rsName, final String dID, final Workspace ws) throws ServiceException {
		traceVerbose("Enter getDocInfo");

		try {
			traceVerbose("rsName: " + rsName);
			final DataBinder serviceBinder = new DataBinder();
			serviceBinder.putLocal(IDC_SERVICE, DOC_INFO);
			serviceBinder.putLocal(D_ID, dID);
			executeService(serviceBinder, ws);
			final DataResultSet drs = new DataResultSet();
			drs.copy(serviceBinder.getResultSet(rsName));
			return drs;
		} finally {
			trace("End getDocInfo");
		}
	}

	/**
	 * Call UPDATE_DOCINFO for dID from originalDataBinder and set folderField
	 * to newFolderValue.
	 * 
	 * @param originalDataBinder
	 *            Original DataBinder from filter.
	 * @param folderField
	 *            xCollectionID or fFolderGUID.
	 * @param newFolder
	 *            New folder field value.
	 * @param ws
	 *            Workspace from filter.
	 */
	static void updateMetadata(final DataBinder originalDataBinder, final String folderField, final String newFolderValue, final Workspace ws) {
		traceVerbose("Enter updateMetadata");

		try {

			final DataBinder serviceBinder = new DataBinder();

			// Necessary metadata for UPDATE_DOCINFO
			final String[] metadata = new String[] { D_DOC_NAME, D_ID, D_REV_LABEL, D_SECURITY_GROUP, D_DOC_ACCOUNT };

			for (final String field : metadata) {
				final String fieldValue = originalDataBinder.getLocal(field);
				if (fieldValue != null && !fieldValue.trim().isEmpty()) {
					serviceBinder.putLocal(field, fieldValue);
				}
			}

			try {
				serviceBinder.putLocal(folderField, newFolderValue);
				serviceBinder.putLocal(IDC_SERVICE, ServiceName.UPDATE_DOCINFO.name());

				executeService(serviceBinder, ws);

			} catch (final ServiceException e) {
				warn("Could not update metadata for dID: " + serviceBinder.getLocal(D_ID), e);
			}
		} finally {
			traceVerbose("Exit updateMetadata");
		}
	}

	/**
	 * Verbose trace output to the WebCenter Content console.
	 * 
	 * @param message
	 *            The String to trace to the WebCenter Content console.
	 */
	static void trace(final String message) {
		Report.trace(TRACE_SECTION, message, null);
	}

	/**
	 * Verbose trace output to the WebCenter Content console.
	 * 
	 * @param message
	 *            The String to trace to the WebCenter Content console.
	 */
	static void traceVerbose(final String message) {
		if (Report.m_verbose) {
			trace(message);
		}
	}

	/**
	 * Output a warning message to the WebCenter Content log.
	 * 
	 * @param message
	 *            The String to output to the WebCenter Content log.
	 */
	static void warn(final String message, final Exception exception) {
		Report.warning(TRACE_SECTION, message, exception);
	}
}
