package com.mythics.webcenter.content.workflowupdatefolder;

import static com.mythics.webcenter.content.workflowupdatefolder.StringConstants.COMPONENT_ENABLED;
import static com.mythics.webcenter.content.workflowupdatefolder.Utils.doCheckAndUpdate;
import static com.mythics.webcenter.content.workflowupdatefolder.Utils.trace;
import static com.mythics.webcenter.content.workflowupdatefolder.Utils.traceVerbose;
import static intradoc.shared.SharedObjects.getEnvValueAsBoolean;
import intradoc.common.ExecutionContext;
import intradoc.data.DataBinder;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradoc.shared.FilterImplementor;

import com.mythics.webcenter.content.workflowupdatefolder.StringConstants.ServiceName;

/**
 * 
 * @author Jonathan Hult
 * 
 */
public class ServiceCleanUpFilter implements FilterImplementor {

	public int doFilter(final Workspace ws, final DataBinder binder, final ExecutionContext ctx) {
		traceVerbose("Enter doFilter for serviceCleanUp for WorkflowUpdateFolder");

		try {
			if (getEnvValueAsBoolean(COMPONENT_ENABLED, false)) {
				if (ctx instanceof Service) {
					final String serviceName = ((Service) ctx).getServiceData().m_name;
					trace("Service name: " + serviceName);

					// Only execute during service WORKFLOW_APPROVE
					if (serviceName != null && serviceName.startsWith(ServiceName.WORKFLOW_APPROVE.name())) {
						traceVerbose("Found proper service: " + ServiceName.WORKFLOW_APPROVE.name());
						doCheckAndUpdate(binder, ws);
					}
				}
			}
		} finally {
			traceVerbose("Exit doFilter for serviceCleanUp for WorkflowUpdateFolder");
		}
		return CONTINUE;
	}

}
