package com.mythics.webcenter.content.workflowupdatefolder;

public class StringConstants {
	/**
	 * 
	 * @author Jonathan Hult
	 * 
	 */

	enum ServiceName {
		UPDATE_DOCINFO, WORKFLOW_APPROVE
	}

	// Component
	static final String COMPONENT_ENABLED = "WorkflowUpdateFolder_ComponentEnabled";
	static final String TRACE_SECTION = "workflowupdatefolder";
	static final String FOLDER_FIELD = "WorkflowUpdateFolder_FolderField";

	// WebCenter
	static final String IDC_SERVICE = "IdcService";
	static final String REMOTE_USER = "REMOTE_USER";
	static final String SYSADMIN = "sysadmin";

	// Metadata
	static final String D_DOC_NAME = "dDocName";
	static final String D_ID = "dID";
	static final String D_REV_LABEL = "dRevLabel";
	static final String D_SECURITY_GROUP = "dSecurityGroup";
	static final String D_DOC_ACCOUNT = "dDocAccount";
	static final String DOC_INFO = "DOC_INFO";
	static final String REVISION_HISTORY = "REVISION_HISTORY";
}
